# Version 20180225
# Pull from CentOS RPM Build Image
FROM centos

MAINTAINER Edzo Botjes <e.a.botjes@gmail.com>

# Update the image
RUN yum -y install deltarpm
RUN yum -y update

# Install Apache
RUN yum -y install httpd

# Create Content WebServer
RUN echo "<p>Hello Apache server on CentOS Docker </p>" > /var/www/html/index.html && \
    echo "<p>$(cat /etc/centos-release) </p>" >> /var/www/html/index.html && \
    echo "<p>$(apachectl -v) </p>" >> /var/www/html/index.html
RUN mkdir /var/www/html/test -p && touch /var/www/html/test/hi && touch /var/www/html/test/hello 

# Cleanup
RUN yum clean all && rm -rf /var/cache/yum

EXPOSE 80 
CMD /usr/sbin/apachectl -DFOREGROUND