# CentOS Apache
This repository is a simple centOS container.  
CentOS Base install updated via yum update.   
Apache (only) installed.

**Table of Contents** 
- [CentOS Apache](#centos-apache)
- [TL;DR](#tl;dr)
- [Getting Started](#getting-started)
  - [Port binding container -> host.](#port-binding-container-->-host.)
  - [Port binding host -> internet facing ip](#port-binding-host-->-internet-facing-ip)
  - [Prerequisites)](#prerequisites)
  - [Run it](#run-it)
    - [Configure port binding](#configure-port-binding)
    - [Play around](#play-around)
    - [Test Directory](#test-directory)
    - [Result](#result)
  - [Stop it](#stop-it) 
- [Built With](#built-with)
  - [Devices](#devices)
  - [CodeEditor](#codeeditor)
  - [Programming Language(s)](#programming-language(s))
  - [Tools](#tools)
- [Contributing](#contributing)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)
  - [Inspiration on Dockerfile](#inspiration-on-dockerfile)
  - [Inspiration on (Apache) containers](#Inspiration on (Apache) containers)
  - [Inspiration on supervisord](#Inspiration on supervisord)
  - [Inspiration on Readme.md](#inspiration-on-readme.md)
  - [Inspiration on Markdown Syntax](#inspiration-on-markdown syntax)

## TL;DR
`docker-compose build`

`docker-compose up`

`lynx localhost:80`

`docker-compose down`

## Getting Started
This code will give you a docker container with the most recent CentOS, updated and apache base installed.

### port binding container -> host.
Apache is a webserver that default binds to port 80.   
When it runs in your container,   
most default it so bind the container port to the host port,  
so that you can reach the apache in the container from outside of the host.  

### port binding host -> internet facing ip
In the case of running on a Google Cloud VM,    
this implies that binding the container port to the VM port,   
that you can reach the sites served by the container via the external ip of the VM    
and thus from everywhere on the internet.

### Prerequisites
see Gitlab [centOS-Updated](https://gitlab.com/edzob/centOS-updated) for instructions and documentation
1. You need a machine with docker and it would help to also have docker-compose installed.
  1. Select place for running docker daemon
  1. Setup VM (gitlab-keys & docker & docker-compose)

### Run it
1. Login on your vm
1. go to the project dir for example    
`cd Projects`
1. clone the project repository    
`git clone https://gitlab.com/edzob/centOS-apache` 
1. go to the repository dir   
`cd centOs-apache`
1. build the container via docker compose.    
`docker-compose build`
1. run the container (spin up)
`docker-compose up`

#### Configure port binding
Edit the docker-compose.yml to map the container port 80 to a different host port.    
Do not forget that there is probably a firewall between your host externalIP address and the internet, 
so random choosing a port on the VM will not always result in something positive.

#### Play around
1. access apache from the VM via :   
`lynx localhost:80`    
lynx is a text based browser that also is known as Lynx. 
1. access apache from the externip of the vm. Go with a browser to    
http://external-ip:80
1. go into the container and play around.    
`docker-compose run --rm centos-apache /bin/bash`

#### Test Directory
The container also inludes a directory in the webroot with two files.   
You can view the directory listing via the following URL.    
`http://external-ip:80/test`    
`lynx localhost:80/test`    

#### Result
When you reach the homepage of the container, you should see something similar to:

```
Hello Apache server on CentOS Docker

CentOS Linux release 7.4.1708 (Core)

Server version: Apache/2.4.6 (CentOS) Server built: Oct 19 2017 20:39:16
```

When you reach the directory listing, you should see something similar to: 

```
Index of /test
[ICO]	Name	Last modified	Size	Description
[PARENTDIR]	Parent Directory	 	-	 
[TXT]	hello	2018-02-25 10:26	0	 
[TXT]	hi	2018-02-25 10:26	0	 

```
### Stop it
`docker-compose down`

## Built With
### Devices
* Chromebook
* Google Cloud Computing

### CodeEditor
* Chromebook
    * [Cloud9](https://ide.c9.io/) Code editor
* Google cloud
    * vi

### Programming Language(s)
* Bash
* Dockerfile
* YML

### Tools
* [Docker](https://docs.docker.com)
* [Docker-compose](https://docs.docker.com/compose)
* [Lynx](http://lynx.browser.org/)
    * Alternate : [elinks](http://elinks.or.cz/) 

## Contributing

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Edzo Botjes** - *Initial work* - [Gitlab](https://gitlab.com/edzob), [Blogger](http://blog.edzob.com/), [Medium](https://medium.com/@edzob), [LinkedIN](https://www.linkedin.com/in/edzob/), [twitter](https://twitter.com/edzob) 

See also the list of [contributors](https://gitlab.com/edzob/centOS-apache/graphs/master) who participated in this project.

## License
```
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   
#    For more see the file 'LICENSE' for copying permission.
```

## Acknowledgments
### Inspiration on Dockerfile
* Apache is run by the [CMD][docker01] operator in the dockerfile and dockercompose file. 

### Inspiration on (Apache) containers
* "[A Practical Introduction to Docker Containers][dockerRHEL01]" written byScott McCarty and Joe Brockmeier
  * This is a great and broad introduction to containers.
* [Bitnami][bitnami02] is a library of installers or software packages for web applications and development stacks 
as well as virtual appliances. On their [github repository][bitnami01] is a nice overview on their apache container.
And how to configure / operate and utilize them.
* "[Building a Simple Apache Web Server in a Container][dockerRHEL08]" written by Redhat
  * Step by Step guide to get an container with Apache running under RedHat. 

### Inspiration on supervisord
* "[CentOS Dockerfile for Apache httpd][dockerRHEL05]" written by the CentOS project
  * a dockercontainer with CentOS and httpd 
* "[CentOS-7 with supervisord launcher | Docker][dockerRHEL06]" written by m12.io (million12)
  * an example of the installation of supervisord and the usage of this in a container. Inspiration for when expanding on the existing container.
* "[Running Multiple Daemon Processes in Docker][dockerRHEL07]" written by Tibor's Musings
  * quick eample of running multile daemon processes in one container.

[dockerRHEL08]: https://access.redhat.com/articles/1328953
[dockerRHEL07]: http://tiborsimko.org/docker-running-multiple-processes.html
[dockerRHEL06]: https://github.com/million12/docker-centos-supervisor/blob/master/Dockerfile
[dockerRHEL05]: https://github.com/CentOS/CentOS-Dockerfiles/tree/master/httpd/centos7
[dockerRHEL01]: https://developers.redhat.com/blog/2014/05/15/practical-introduction-to-docker-containers/

[bitnami01]: https://github.com/bitnami/bitnami-docker-apache
[bitnami02]: https://en.wikipedia.org/wiki/Bitnami
[docker01]: https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#cmd

### Inspiration on Readme.md
* Inspiration on Readme.md
  * Github user PurpleBooth wrote: "[A template to make good README.md][readme01]"
  * Github user jxson wrote: "[README.md template][readme02]"
  * Medium user meakaakka wrote: "[A Beginners Guide to writing a Kickass README][readme03]"

### Inspiration on Markdown Syntax
* Inspiration on Markdown Syntax
    * GitHub   
      * Github user __adam-p__ wrote: "[Markdown Cheatsheet][markdown01]"
      * __Github Guides__ wrote: "[Markdown Syntax (pdf)][markdown02]"
      * __Github Guides__ wrote: "[Mastering Markdown (Wiki)][markdown03]"
      * Github user __tchapi__ wrote: "[Markdown Cheatsheet for Github][markdown04]"
    * GitLab
      * __GitLab documentation__ - Markdown wrote: "[GitLab Flavored Markdown (GFM)][markdown05]"
      * __GitLab Team Handbook__ - Markdown Guide wrote: "[Markdown kramdown Style Guide for about.GitLab.com][markdown06]"

[readme01]: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
[readme02]: https://gist.github.com/jxson/1784669
[readme03]: https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3

[markdown01]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[markdown02]: https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf
[markdown03]: https://guides.github.com/features/mastering-markdown/ 
[markdown04]: https://github.com/tchapi/markdown-cheatsheet

[markdown05]: https://docs.gitlab.com/ee/user/markdown.html
[markdown06]: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/